package com.vegastower.db;

import java.util.Collection;
import java.util.List;

import org.springframework.data.couchbase.core.query.Query;
import org.springframework.data.couchbase.core.query.View;
import org.springframework.data.repository.CrudRepository;

import com.vegastower.domain.oceanpearl.OceanPearlSlotDoc;

public interface TestRepository extends CrudRepository<TestDoc, String>
{
	@View(designDocument = "slot", viewName = "byId")
	List<OceanPearlSlotDoc> findByIdBetween(int minId, int maxId);
	
	@View(designDocument = "slot", viewName = "byId")
	List<OceanPearlSlotDoc> findById(String Id);
	
	@View(designDocument = "slot", viewName = "byName")
	List<OceanPearlSlotDoc> findByName(String name);
	
	@Query
    List<OceanPearlSlotDoc> findByNo(int no);
	
//	@Query
//	Collection<Object> findByUID(int uid);
}
