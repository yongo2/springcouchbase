package com.vegastower.db;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;

public class TestDoc {
	@Id
	public String id = "BEER_TEST";
	
	@Field
	private int no;

	public int getNo() { return this.no; }

	public void setNo(int no) { this.no = no; }

	@Field
	private String name;

	public String getName() { return this.name; }

	public void setName(String name) { this.name = name; }
}
