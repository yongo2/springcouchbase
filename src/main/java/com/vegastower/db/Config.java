package com.vegastower.db;

import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.couchbase.config.AbstractCouchbaseConfiguration;
import org.springframework.data.couchbase.repository.config.EnableCouchbaseRepositories;

//import com.vegastower.couchbase.Config;

@Configuration
@EnableCouchbaseRepositories
public class Config extends AbstractCouchbaseConfiguration {

	private static final Logger logger = LoggerFactory.getLogger(Config.class);
    @Override
    protected List<String> getBootstrapHosts() {
    	logger.info("DB-getBootstrapHosts");
        //return Arrays.asList("127.0.0.1");
    	//return Collections.singletonList("192.168.99.100");
    	return Collections.singletonList("127.0.0.1");
    }

    @Override
    protected String getBucketName() {
    	logger.info("DB-getBucketName");
        return "beer-sample";
    }

    @Override
    protected String getBucketPassword() {
    	logger.info("DB-getBucketPassword");
        return "";
    }
}