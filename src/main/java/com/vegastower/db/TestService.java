package com.vegastower.db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.couchbase.client.java.Bucket;
import com.vegastower.couchbase.Config2;

@Service("TestService")
public class TestService {
	private static final Logger logger = LoggerFactory.getLogger(TestService.class);
	
	@Autowired
    private Config2 confg;
	private final TestRepository testRepository;

    @Autowired
    public TestService(TestRepository testRepository) {
        this.testRepository = testRepository;
    }
    
	
    public void doWork() {
    	logger.info("TestService");    	
    
    	try {
    		TestDoc doc = new TestDoc();
    		doc.setName("dd");
    		doc.setNo(123123);
    		testRepository.save(doc);
    	}
    	catch (Exception e) 
    	{
    		logger.info(e.toString());
    	}
    }
	
    public void doLoad() {
    	logger.info("Load");    	
    
    	try {
			Bucket bucket = confg.couchbaseCluster().openBucket("beer-sample");
			logger.info( bucket.toString() );
			logger.info( bucket.async().get("21st_amendment_brewery_cafe").toString() );
			
			
    	}
    	catch (Exception e) 
    	{
    		logger.info(e.toString());
    	}
    }
	
	
}
