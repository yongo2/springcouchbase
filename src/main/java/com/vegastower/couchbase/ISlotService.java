package com.vegastower.couchbase;

import java.io.File;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vegastower.domain.ISlotDocument;
import com.vegastower.domain.oceanpearl.OceanPearlSlotDoc;

@Service
public class ISlotService {

	private static final Logger logger = LoggerFactory.getLogger(ISlotService.class);
    private final ISlotRepository slotRepository;

    //private final ISlotRepository islotRepository;
    
    @Autowired
    public ISlotService(ISlotRepository slotRepository) {
        this.slotRepository = slotRepository;
    }

    
    
    //oceanpearl
	public OceanPearlSlotDoc LoadDoc(String path) throws Exception {
		ClassLoader classLoader = getClass().getClassLoader();
		ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
		return mapper.readValue(new File(classLoader.getResource(path).getFile()), OceanPearlSlotDoc.class);
	}
	
    public void doWork() {
    	logger.info("ISlotService");
    	
    
    	try {
    		OceanPearlSlotDoc slotdoc = LoadDoc("oceanpearl.json");
    		slotdoc.setName("yongo3");
    		slotdoc.setNo(123123);
    		
    		slotdoc.id = slotdoc.id + "::"+slotdoc.getName(); 
    		slotdoc = slotRepository.save(slotdoc);
    	}
    	catch (Exception e) 
    	{
    		logger.info(e.toString());
    	}
    	//OceanPearlSlotDoc slotdoc = new OceanPearlSlotDoc();
    	
    	
    }
    
    public void doLoad() {
    	logger.info("load");    	
    
    	try {    		 
    		Iterable<ISlotDocument> slotdocList = slotRepository.findByName("yongo");
    				//.findByName("oceanpearl");
    				//findById("SLOT");
    		for(ISlotDocument o : slotdocList )
    			logger.info(o.id  + "::" + o.getName());
    		//slotdoc = slotRepository.save(slotdoc);
    	}
    	catch (Exception e) 
    	{
    		logger.info(e.toString());
    	}
    }
    
    public void doLoad3() {
    	logger.info("load");    	
    
    	try {  
    		
    		// Unable to execute query due to the following n1ql errors: 
    		// {"msg":"No primary index on keyspace default. Use CREATE PRIMARY INDEX to create one.","code":4000}
    		Iterable<ISlotDocument> slotdocList = slotRepository.findByNoBetween(0, 999999);
    				//.findByName("oceanpearl");
    				//findById("SLOT");
    		for(ISlotDocument o : slotdocList )
    			logger.info(o.id  + "::" + o.getName());
    		//slotdoc = slotRepository.save(slotdoc);
    	}
    	catch (Exception e) 
    	{
    		logger.info(e.toString());
    	}
    }
}
