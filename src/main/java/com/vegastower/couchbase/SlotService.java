package com.vegastower.couchbase;

import java.io.File;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vegastower.domain.oceanpearl.OceanPearlSlotDoc;

@Service
public class SlotService {

	private static final Logger logger = LoggerFactory.getLogger(SlotService.class);
    private final SlotRepository slotRepository;

    //private final ISlotRepository islotRepository;
    
    @Autowired
    public SlotService(SlotRepository slotRepository) {
        this.slotRepository = slotRepository;
    }

    
    
    //oceanpearl
	public OceanPearlSlotDoc LoadDoc(String path) throws Exception {
		ClassLoader classLoader = getClass().getClassLoader();
		ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
		return mapper.readValue(new File(classLoader.getResource(path).getFile()), OceanPearlSlotDoc.class);
	}
	
    public void doWork() {
    	logger.info("SlotService");
    	
    
    	try {
    		OceanPearlSlotDoc slotdoc = LoadDoc("oceanpearl.json");
    		slotdoc.id = slotdoc.id+slotdoc.getNo(); 
    		slotdoc = slotRepository.save(slotdoc);
    	}
    	catch (Exception e) 
    	{
    		logger.info(e.toString());
    	}
    	//OceanPearlSlotDoc slotdoc = new OceanPearlSlotDoc();
    }
    
    public void doLoad() {
    	logger.info("load");    	
    
    	try {    		 
    		List<OceanPearlSlotDoc> slotdocList = slotRepository.findByName("oceanpearl");
    				//findById("SLOT");
    		for(OceanPearlSlotDoc o : slotdocList )
    			logger.info(o.getName());
    		//slotdoc = slotRepository.save(slotdoc);
    	}
    	catch (Exception e) 
    	{
    		logger.info(e.toString());
    	}
    }
    
    public void doLoadBySlotNo(int slotNo) {
    	logger.info("load");    	
    
    	try {
    		//1. 하나 더 넣구 범위 검색 어떻게 되는지.
    		List<OceanPearlSlotDoc> slotdocList = slotRepository.findByNo(slotNo);
    		for(OceanPearlSlotDoc o : slotdocList )
    			logger.info(o.getName());

    		
    	}
    	catch (Exception e) 
    	{
    		logger.info(e.toString());
    	}
    }
    
    
    public void doLoadBySlotNoAndName(int slotNo, String name) {
    	logger.info("doLoadBySlotNoAndName");    	
    
    	try {
    		//1. 하나 더 넣구 범위 검색 어떻게 되는지.
    		List<OceanPearlSlotDoc> slotdocList = slotRepository.findByNoAndName(slotNo, name);
    		for(OceanPearlSlotDoc o : slotdocList )
    		{
    			logger.info(o.getName());
    			logger.info(o.getScatterTrigger().toString());
    		}

    		
    	}
    	catch (Exception e) 
    	{
    		logger.info(e.toString());
    	}
    }
    
    
}
