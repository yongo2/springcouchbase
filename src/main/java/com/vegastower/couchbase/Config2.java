package com.vegastower.couchbase;

import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.couchbase.config.AbstractCouchbaseConfiguration;
import org.springframework.data.couchbase.repository.config.EnableCouchbaseRepositories;

import com.couchbase.client.java.Bucket;

@Configuration
@EnableCouchbaseRepositories
public class Config2 extends AbstractCouchbaseConfiguration {

	private static final Logger logger = LoggerFactory.getLogger(Config2.class);
    @Override
    protected List<String> getBootstrapHosts() {
    	logger.info("getBootstrapHosts");
        //return Arrays.asList("127.0.0.1");
    	//return Collections.singletonList("192.168.99.100");
    	return Collections.singletonList("127.0.0.1");
    }

    @Override
    protected String getBucketName() {
    	logger.info("getBucketName");
        return "default";
    }

    @Override
    protected String getBucketPassword() /*throws Exception */{
    	logger.info("getBucketPassword");
//    	Bucket bk = couchbaseClient();
//    	try
//    	{
//    		Bucket bk = couchbaseCluster().openBucket("", getBucketPassword());
//    	}
//    	catch(Exception e){
//    	//	e;
//    	}
        return "";
    }
}