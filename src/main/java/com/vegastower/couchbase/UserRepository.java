package com.vegastower.couchbase;

import java.util.List;

import org.springframework.data.couchbase.core.query.Query;
import org.springframework.data.couchbase.core.query.View;
import org.springframework.data.repository.CrudRepository;
public interface UserRepository extends CrudRepository<User, String> {

    /**
     * Additional custom finder method, backed by an auto-generated
     * N1QL query.
     */
	@Query
    List<User> findByLastnameAndAgeBetween(String lastName, int minAge,
        int maxAge);

	@Query
    List<User> findByAge(int age);
	
	@Query
    List<User> findByAgeBetween(int minAge, int maxAge);
	
	
	/*
	 * all
	 * function (doc, meta) {
  		if(doc._class == "com.example.entity.UserInfo") {
    		emit(null, null);
  		}
	}
	 * */
	
	
    /**
     * Additional custom finder method, backed by a View that indexes
     * the names.
     *
     * ByName
     * function (doc, meta) {
  			if(doc._class == "com.example.entity.UserInfo" && doc.lastname) {
    		emit(doc.lastname, null);
  			}
		}
     */
    @View(designDocument = "user", viewName = "byName")
    List<User> findByLastname(String lastName);
    
    /**
     * Additional custom finder method, backed by a geospatial view and
     * allowing multi-dimensional queries.
     * You can also query within a Circle or a Polygon.
     */
//    @Dimensional(designDocument = "userGeo", spatialViewName = "byLocation")
//    List<User> findByLocationWithin(Box cityBoundingBox);
    
    @Query("SELECT count(*) FROM default")
    long myCount();
}
