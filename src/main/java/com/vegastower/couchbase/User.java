package com.vegastower.couchbase;

import org.springframework.data.annotation.Id;
import org.springframework.data.couchbase.core.mapping.Document;
import org.springframework.data.geo.Point;

import com.couchbase.client.java.repository.annotation.Field;

@Document(expiry = 0)
public class User {

	@Field  
	private String firstname;
	
	@Id
	private String Id = "User";
	
	@Field  
	private String lastname;
	@Field  
	private int age;
	
		
    @Field  
	private Point location;
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public Point getLocation() {
		return location;
	}
	public void setLocation(Point location) {
		this.location = location;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	
}
