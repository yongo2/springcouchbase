package com.vegastower.couchbase;

import java.util.List;

import org.springframework.data.couchbase.core.query.Query;
import org.springframework.data.couchbase.core.query.View;
import org.springframework.data.repository.CrudRepository;

import com.vegastower.domain.ISlotDocument;
import com.vegastower.domain.oceanpearl.OceanPearlSlotDoc;
public interface SlotRepository extends CrudRepository<OceanPearlSlotDoc, String> 
{
	@View(designDocument = "slot", viewName = "byId")
	List<OceanPearlSlotDoc> findByIdBetween(int minId, int maxId);
	
	@View(designDocument = "slot", viewName = "byId")
	List<OceanPearlSlotDoc> findById(String Id);
	
	@View(designDocument = "slot", viewName = "byName")
	List<OceanPearlSlotDoc> findByName(String name);
	
	@Query	
    List<OceanPearlSlotDoc> findByNo(int no);
	
	
	@Query
    List<OceanPearlSlotDoc> findByNoAndName(int no, String name);
	
    /**
     * Additional custom finder method, backed by an auto-generated
     * N1QL query.
     */
//	@Query
//    List<User> findByLastnameAndAgeBetween(String lastName, int minAge,
//        int maxAge);
//
//	@Query
//    List<User> findByAge(int age);
//	
//	@Query
//    List<User> findByAgeBetween(int minAge, int maxAge);
//    /**
//     * Additional custom finder method, backed by a View that indexes
//     * the names.
//     */
//    @View(designDocument = "user", viewName = "byName")
//    List<User> findByLastname(String lastName);
//
//    /**
//     * Additional custom finder method, backed by a geospatial view and
//     * allowing multi-dimensional queries.
//     * You can also query within a Circle or a Polygon.
//     */
////    @Dimensional(designDocument = "userGeo", spatialViewName = "byLocation")
////    List<User> findByLocationWithin(Box cityBoundingBox);
//    
//    @Query("SELECT count(*) FROM default")
//    long myCount();
}
