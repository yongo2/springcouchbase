package com.vegastower.couchbase;

import java.util.List;

import org.springframework.data.couchbase.core.query.View;
import org.springframework.data.repository.CrudRepository;

import com.vegastower.domain.ISlotDocument;
import com.vegastower.domain.oceanpearl.OceanPearlSlotDoc;
public interface ISlotRepository extends CrudRepository<ISlotDocument, String> {

	@View(designDocument = "slot", viewName = "byId")
	List<ISlotDocument> findById(String Id);
	
	@View(designDocument = "slot", viewName = "byName")
	List<ISlotDocument> findByName(String name);
	
	List<ISlotDocument> findByNoBetween(int minNo, int maxNo);
    /**
     * Additional custom finder method, backed by an auto-generated
     * N1QL query.
     */
//	@Query
//    List<User> findByLastnameAndAgeBetween(String lastName, int minAge,
//        int maxAge);

}
