package com.vegastower.couchbase;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Point;
import org.springframework.stereotype.Service;

@Service("MyService")
public class MyService {

	private static final Logger logger = LoggerFactory.getLogger(MyService.class);
    private final UserRepository userRepository;

    @Autowired
    public MyService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

//	@Autowired
//	private UserRepository userRepository;
	
    public void doWork() {
        //userRepository.deleteAll();

        User user = new User();
        user.setLastname("Jackson");
        user.setFirstname("Yongo");
        user.setLocation(new Point(123, 456));
        user.setAge(15);

        logger.info( "size " + userRepository.myCount() );
        user = userRepository.save(user);

        List<User> jacksonFamily =
                userRepository.findByLastname("Jackson");
            for(User u :jacksonFamily)
            	logger.info("[" + u.getLastname() + "]: "+ u.getAge());

            
        List<User> jf2 = userRepository.findByAge(15);       
            for(User u :jf2)
         	   logger.info("[" + u.getLastname() + "]: "+ u.getAge());
            
            
       List<User> jf = userRepository.findByAgeBetween(0, 15);       
       for(User u :jf)
    	   logger.info("[" + u.getLastname() + "]: "+ u.getAge());
       
       
       
        List<User> jacksonChildren =
            userRepository.findByLastnameAndAgeBetween("Jackson", 0, 18);
        for(User u :jacksonChildren )
        	logger.info("[" + u.getLastname() + "]: "+ u.getAge());
        
        
        
 

        //bounding box is lower-left, upper-right corners
//        Box cityBounds = new Box(new Point(100, 100), new Point(150, 500));
//        List<User> jacksonsInSomeCity =
//            userRepository.findByLocationWithin(cityBounds);
    }
}
