package com.vegastower.domain;

import java.util.ArrayList;

public class IReelTemplate {
	public  ArrayList<String> Reel1;

	public ArrayList<String> getReel1() {
		return this.Reel1;
	}

	public void setReel1(ArrayList<String> Reel1) {
		this.Reel1 = Reel1;
	}

	public  ArrayList<String> Reel2;

	public ArrayList<String> getReel2() {
		return this.Reel2;
	}

	public void setReel2(ArrayList<String> Reel2) {
		this.Reel2 = Reel2;
	}

	public  ArrayList<String> Reel3;

	public ArrayList<String> getReel3() {
		return this.Reel3;
	}

	public void setReel3(ArrayList<String> Reel3) {
		this.Reel3 = Reel3;
	}

	public  ArrayList<String> Reel4;

	public ArrayList<String> getReel4() {
		return this.Reel4;
	}

	public void setReel4(ArrayList<String> Reel4) {
		this.Reel4 = Reel4;
	}

	public  ArrayList<String> Reel5;

	public ArrayList<String> getReel5() {
		return this.Reel5;
	}

	public void setReel5(ArrayList<String> Reel5) {
		this.Reel5 = Reel5;
	}
}
