package com.vegastower.domain;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;

public abstract class ISlotDocument {


	@Id
	public String id = "SLOT";
	
	@Field
	private int no;

	public int getNo() { return this.no; }

	public void setNo(int no) { this.no = no; }

	@Field
	private String name;

	public String getName() { return this.name; }

	public void setName(String name) { this.name = name; }
	
	
}
