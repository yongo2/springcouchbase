package com.vegastower.domain.oceanpearl;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

import com.vegastower.domain.IReelTemplate;
import com.vegastower.domain.ISlotDocument;

public class OceanPearlSlotMachine {

	private OceanPearlSlotDoc slotDoc;
	public void SetSlotDoc(ISlotDocument slotDoc)  throws Exception
	{		
		this.slotDoc = (OceanPearlSlotDoc) slotDoc;
		
		Load();
	}

	
	
	

	public void Reset()
	{
		this.slotDoc = null;
		
		m_payLines.clear();
		m_reelSet.clear();
		m_reelList.clear();
	}
	
	static final String REEL_TYPE_NORMAL = "normal";
	static final String REEL_TYPE_FREE = "free";
	static final String REEL_TYPE_NORMAL_CLIENT = "normalClient";
	static final String REEL_TYPE_FREE_CLIENT = "freeClient";
	public void Load() throws Exception
	{
		// payline to OceanPearl payline
		for( com.vegastower.domain.oceanpearl.PayLine p : slotDoc.getPayLines())
		{
			OceanPearlPayLine inner = new OceanPearlPayLine();
			inner.Load(p);
			m_payLines.add(inner);	
		}
	
		// Reel List
		String methodName;
		Method method;
		for( ReelList reellist : slotDoc.getReelList() )
		{
			methodName = "get"+ reellist.getName();				 
			method=slotDoc.getClass().getMethod(methodName);
			IReelTemplate reelTemplte= (IReelTemplate)method.invoke(slotDoc);
			
			
			OceanPearlReel innerReel = GetInnerReel(reellist.getName());
			for (int reel_pos = 1; reel_pos < 6; ++reel_pos) 
			{
				methodName = "getReel"+ reel_pos;				 
				method=reelTemplte.getClass().getMethod(methodName);
				ArrayList<String> symbol_list = (ArrayList<String>)method.invoke(reelTemplte);
				
				innerReel.getReelinfo().add(symbol_list);				
			}		
		}
		
		// Reel Set. 4가지 종유가 있따.
		for( ReelSet reelset : slotDoc.getReelSets() )
		{
			HashMap<String, String> bind2Name = new HashMap<String, String> ();
			bind2Name.put(REEL_TYPE_NORMAL, reelset.getNormal());
			bind2Name.put(REEL_TYPE_FREE, reelset.getFree());
			bind2Name.put(REEL_TYPE_NORMAL_CLIENT, reelset.getNormalClient());
			bind2Name.put(REEL_TYPE_FREE_CLIENT, reelset.getFreeClient());
			m_reelSet.put(reelset.getType(), bind2Name);
		}
						
	}
	
	private ArrayList<OceanPearlPayLine> m_payLines = new ArrayList<OceanPearlPayLine> ();
	public ArrayList<OceanPearlPayLine> GetPayLines() {
		return m_payLines;
	}
	
	private HashMap<String, HashMap<String, String>> m_reelSet = new HashMap<String, HashMap<String, String>> ();
	private HashMap<String, OceanPearlReel> m_reelList = new HashMap<String, OceanPearlReel> ();
	
	public HashMap<String, OceanPearlReel> GetInnerReelList() { 
		return m_reelList; 
	}
	
	public OceanPearlReel GetInnerReel(String name)
	{
		if(!m_reelList.containsKey(name))
			m_reelList.put(name, new OceanPearlReel());
	
		return m_reelList.get(name);
	}
	
	public OceanPearlReel GetInnerReel(String setType, String reelType)
	{
		// real: normal, free, normalClient, freeClient
		return GetInnerReel( GetReelName(setType, reelType) );
	}
	
	public String GetReelName( String setType, String reelType )
	{
		return m_reelSet.get(setType).get(reelType);
	}

	public int GetReelSize( String setType, String reelType, int index)
	{
		return GetInnerReel(setType, reelType).getReelinfo().get(index).size();
	}

	
	
	public SlotSymbol ConvertSymbolByID(String symbolName) 
	{
		for (SlotSymbol s : slotDoc.getSlotSymbol()) 
		{
			if (s.getId().equals(symbolName))
				return s;
		}

		
		return null;
	}
	
	public SlotSymbol ConvertSymbolByType(String symbolType) 
	{		
		for (SlotSymbol s : slotDoc.getSlotSymbol()) 
		{
			if (s.getType().equals(symbolType))
				return s;
		}
		//logger.error("CANNOT FIND" + symbolType);
		return null;
	}
	
	
	public ScatterTrigger GetScatterTrigger() {
		return slotDoc.getScatterTrigger();
	}

	public boolean IsScatterTrigger(int index)
	{
		if(0==slotDoc.getScatterBonus().get(index).getIsMoreBonus())
			return false;
		return true;
	}
	
	public int GetScatterBonusMultiply(int index) 
	{
		return slotDoc.getScatterBonus().get(index).getMultiplyChip(); 
	}
	public Info GetInfo()
	{
		return slotDoc.getInfo();
	}

}





