package com.vegastower.domain.oceanpearl;

public class ReelSet
{
  private String type;

  public String getType() { return this.type; }

  public void setType(String type) { this.type = type; }

  private String normal;

  public String getNormal() { return this.normal; }

  public void setNormal(String normal) { this.normal = normal; }

  private String free;

  public String getFree() { return this.free; }

  public void setFree(String free) { this.free = free; }

  private String normal_client;

  public String getNormalClient() { return this.normal_client; }

  public void setNormalClient(String normal_client) { this.normal_client = normal_client; }

  private String free_client;

  public String getFreeClient() { return this.free_client; }

  public void setFreeClient(String free_client) { this.free_client = free_client; }
}