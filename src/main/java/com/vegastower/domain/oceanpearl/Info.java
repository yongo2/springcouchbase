package com.vegastower.domain.oceanpearl;

import com.couchbase.client.java.repository.annotation.Field;

public class Info {
	
	  @Field
	  private int reelLength;

	  public int getReelLength() { return this.reelLength; }

	  public void setReelLength(int reelLength) { this.reelLength = reelLength; }

	  @Field
	  private int reelHeight;

	  public int getReelHeight() { return this.reelHeight; }

	  public void setReelHeight(int reelHeight) { this.reelHeight = reelHeight; }
}
