package com.vegastower.domain.oceanpearl;

import java.util.ArrayList;

public class SlotSymbol {
	private String id;

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	private int index;

	public int getIndex() {
		return this.index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	private ArrayList<Integer> pay;

	public ArrayList<Integer> getPay() {
		return this.pay;
	}

	public void setPay(ArrayList<Integer> pay) {
		this.pay = pay;
	}

	private String type;

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
