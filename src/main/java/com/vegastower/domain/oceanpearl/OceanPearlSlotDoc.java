package com.vegastower.domain.oceanpearl;

import java.util.ArrayList;

import org.springframework.data.couchbase.core.mapping.Document;

import com.couchbase.client.java.repository.annotation.Field;
import com.vegastower.domain.ISlotDocument;

@Document(expiry = 0)
public class OceanPearlSlotDoc extends ISlotDocument {
	
	
	@Field
	private Info info;

	public Info getInfo() {
		return this.info;
	}

	public void setInfo(Info info) {
		this.info = info;
	}

	@Field
	private ArrayList<ScatterBonu> scatterBonus;

	public ArrayList<ScatterBonu> getScatterBonus() {
		return this.scatterBonus;
	}

	public void setScatterBonus(ArrayList<ScatterBonu> scatterBonus) {
		this.scatterBonus = scatterBonus;
	}

	@Field
	private ScatterTrigger scatterTrigger;

	public ScatterTrigger getScatterTrigger() {
		return this.scatterTrigger;
	}

	public void setScatterTrigger(ScatterTrigger scatterTrigger) {
		this.scatterTrigger = scatterTrigger;
	}

	@Field
	private ArrayList<SlotSymbol> slotSymbol;

	public ArrayList<SlotSymbol> getSlotSymbol() {
		return this.slotSymbol;
	}

	public void setSlotSymbol(ArrayList<SlotSymbol> slotSymbol) {
		this.slotSymbol = slotSymbol;
	}

	@Field
	private ArrayList<PayLine> payLines;

	public ArrayList<PayLine> getPayLines() {
		return this.payLines;
	}

	public void setPayLines(ArrayList<PayLine> payLines) {
		this.payLines = payLines;
	}

	@Field
	private ArrayList<ReelSet> reelSets;

	public ArrayList<ReelSet> getReelSets() {
		return this.reelSets;
	}

	public void setReelSets(ArrayList<ReelSet> reelSets) {
		this.reelSets = reelSets;
	}
	
	@Field	
	private ArrayList<ReelList> reelList;
	
	public ArrayList<ReelList> getReelList() { return this.reelList; }
	
	public void setReelList(ArrayList<ReelList> reelList) { this.reelList = reelList; }
	
	@Field
	private NormalReel normalReel;

	public NormalReel getNormalReel() {
		return this.normalReel;
	}

	public void setNormalReel(NormalReel normalReel) {
		this.normalReel = normalReel;
	}

	@Field
	private FreeReel freeReel;

	public FreeReel getFreeReel() {
		return this.freeReel;
	}

	public void setFreeReel(FreeReel freeReel) {
		this.freeReel = freeReel;
	}

	@Field
	private NormalClientReel normalClientReel;

	public NormalClientReel getNormalClientReel() {
		return this.normalClientReel;
	}

	public void setNormalClientReel(NormalClientReel normalClientReel) {
		this.normalClientReel = normalClientReel;
	}

	@Field
	private FreeClientReel freeClientReel;

	public FreeClientReel getFreeClientReel() {
		return this.freeClientReel;
	}

	public void setFreeClientReel(FreeClientReel freeClientReel) {
		this.freeClientReel = freeClientReel;
	}
}
