package com.vegastower.domain.oceanpearl;

import java.util.ArrayList;

public class ScatterTrigger {
	  private ArrayList<Integer> freeSpin;

	  public ArrayList<Integer> getFreeSpin() { return this.freeSpin; }

	  public void setFreeSpin(ArrayList<Integer> freeSpin) { this.freeSpin = freeSpin; }

	  private ArrayList<Integer> multipleWild;

	  public ArrayList<Integer> getMultipleWild() { return this.multipleWild; }

	  public void setMultipleWild(ArrayList<Integer> multipleWild) { this.multipleWild = multipleWild; }
}
