package com.vegastower.domain.oceanpearl;

import java.lang.reflect.Method;
import java.util.ArrayList;
import com.vegastower.domain.oceanpearl.PayLine;

public class OceanPearlPayLine {

	public void Load(PayLine _payline) throws Exception
	{
		this.payline = _payline;
		String methodName;
		Method method;
		for( int pos=1; pos<6; ++pos)
		{
			methodName = "getReel"+ pos;
			method = payline.getClass().getMethod(methodName);
			ArrayList<Integer> eachPayline = (ArrayList<Integer>)method.invoke(payline);
			paylineList.add( eachPayline );
		}
		
	}
	
	public int getIndex()
	{
		return payline.getIndex();
	}
	
	public ArrayList<Integer> GetPayLinePosition(int index)
	{
		return paylineList.get(index);
	}
	
	
	private PayLine payline;
	private ArrayList<ArrayList<Integer>> paylineList = new ArrayList<ArrayList<Integer>>();	
	public ArrayList<ArrayList<Integer>> GetPaylineList() { return paylineList;} 
}
