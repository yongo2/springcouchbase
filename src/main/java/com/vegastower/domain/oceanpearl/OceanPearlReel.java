package com.vegastower.domain.oceanpearl;

import java.util.ArrayList;

public class OceanPearlReel {

	private ArrayList<ArrayList<String>> reelinfo = new ArrayList<ArrayList<String>>();

	public ArrayList<ArrayList<String>> getReelinfo() {
		return this.reelinfo;
	}

	public void setReelinfo(ArrayList<ArrayList<String>> reelinfo) {
		this.reelinfo = reelinfo;
	}
}
