package com.vegastower.domain.oceanpearl;

import java.util.ArrayList;

public class PayLine {
	private int index;

	public int getIndex() {
		return this.index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	private ArrayList<Integer> reel1;

	public ArrayList<Integer> getReel1() {
		return this.reel1;
	}

	public void setReel1(ArrayList<Integer> reel1) {
		this.reel1 = reel1;
	}

	private ArrayList<Integer> reel2;

	public ArrayList<Integer> getReel2() {
		return this.reel2;
	}

	public void setReel2(ArrayList<Integer> reel2) {
		this.reel2 = reel2;
	}

	private ArrayList<Integer> reel3;

	public ArrayList<Integer> getReel3() {
		return this.reel3;
	}

	public void setReel3(ArrayList<Integer> reel3) {
		this.reel3 = reel3;
	}

	private ArrayList<Integer> reel4;

	public ArrayList<Integer> getReel4() {
		return this.reel4;
	}

	public void setReel4(ArrayList<Integer> reel4) {
		this.reel4 = reel4;
	}

	private ArrayList<Integer> reel5;

	public ArrayList<Integer> getReel5() {
		return this.reel5;
	}

	public void setReel5(ArrayList<Integer> reel5) {
		this.reel5 = reel5;
	}
}
