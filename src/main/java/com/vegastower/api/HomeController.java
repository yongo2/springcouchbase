package com.vegastower.api;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.couchbase.client.java.Bucket;
import com.vegastower.couchbase.Config2;
import com.vegastower.couchbase.ISlotService;
import com.vegastower.couchbase.MyService;
import com.vegastower.couchbase.SlotService;
import com.vegastower.db.TestService;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	

	@Autowired
    private MyService myService;
	
	@Autowired
    private SlotService slotService;
	
	@Autowired
    private ISlotService islotService;
    
	@Autowired
    private TestService testService;
	
	
	@Autowired
    private Config2 confg;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		myService.doWork();		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		model.addAttribute("serverTime", formattedDate );
		
		return "home";
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.GET)
	public String slotSave(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		slotService.doWork();
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		model.addAttribute("serverTime", formattedDate );
		
		return "home";
	}
	
	@RequestMapping(value = "/load", method = RequestMethod.GET)
	public String slotLoad(Locale locale, Model model) {
		logger.info("Load {}.", locale);
		
		slotService.doLoad();
		return "home";
	}
	
	@RequestMapping(value = "/loadbyno", method = RequestMethod.GET)
	public String slotLoad2(Locale locale, Model model) {
		logger.info("Load {}.", locale);
		
		slotService.doLoadBySlotNo(99999);
		return "home";
	}
	
	@RequestMapping(value = "/loadbyno2", method = RequestMethod.GET)
	public String slotLoadQuery(Locale locale, Model model) {
		logger.info("Load {}.", locale);
		
		slotService.doLoadBySlotNoAndName(99999, "testoceanpearl");
		return "home";
	}
	
	/////////////////////////////////////////////////
	// iSlotService
	/////////////////////////////////////////////////
	@RequestMapping(value = "/load3", method = RequestMethod.GET)
	public String slotLoad3(Locale locale, Model model) {
		logger.info("Load {}.", locale);
		
		islotService.doLoad();
		return "home";
	}	
	
	@RequestMapping(value = "/load4", method = RequestMethod.GET)
	public String slotLoad4(Locale locale, Model model) {
		logger.info("Load {}.", locale);
		
		islotService.doLoad3();
		return "home";
	}
	
	/////////////////////////////////////////////////
	// iSlotService
	/////////////////////////////////////////////////
	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public String callTest_11(Locale locale, Model model) {
		logger.info("Load {}.", locale);
		
		try{
			
//			Bucket bucket = confg.couchbaseCluster().openBucket("beer-sample");
//			logger.info( bucket.toString() );
//			logger.info( bucket.async().get("21st_amendment_brewery_cafe").toString() );
			testService.doWork();
		}		
		catch( Exception e)
		{
			logger.error(e.toString());
		}
		return "home";
	}	
	
	
}
